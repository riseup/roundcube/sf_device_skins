<?php
$labels = array();
$labels['current_device'] = "Current device"; 
$labels['legend'] = "Skin Selection: click on the image for avaible themes";
$labels['desktop'] = "Desktop";
$labels['tablet'] = "Tablet";
$labels['phone'] = "Phone";
$labels['select_desktop_skin'] = "Select desktop skin";
$labels['select_tablet_skin'] = "Select tablet skin";
$labels['select_phone_skin'] = "Select phone skin";