<?php
/*
	
*/

class sf_device_skins extends rcube_plugin {

	// Some roundcube plugins have problems with some skins. We are going to 
	// make the plugins belive they are using larry when this happens. If you 
	// find another problematic skins, add them here.
	
	private $problematic_skins = array(

	);
	private $problematic_plugins = array(
		"calendar",
		"calendar_plus",
		"compose_in_taskbar",
		"jappix4roundcube",
		"keyboard_shortcuts",
		"message_highlight",
		"moreuserinfo",
		"nabble",
		"persistent_login",
		"planner",
		"plugin_manager",
		"settings",
		"timepicker",
		"summary",
		"vcard_send",
	);

	private $phone = false;
	private $tablet = false;
	private $desktop = false;
	private $mobile = false;
	private $device = false;
	private $skin = false;


	// init
	public function init() {
		$this->set_device();
		$this->add_hook("config_get", array($this, "sf_get_config"));
		$this->add_hook("startup", array($this, 'sf_start'));
		$this->add_hook("render_page", array($this, 'sf_render_page'));
		$this->add_hook("preferences_list", array($this, 'sf_preferences_list'));
		$this->add_hook("preferences_save", array($this, 'sf_preferences_save'));
		$this->include_stylesheet("styles.css");
		$this->include_script("scripts.js");

		if ($this->mobile) {
		    global $CONFIG;
		    $CONFIG['htmleditor'] = false;
		    $CONFIG['prefer_html'] = false;
		}
	}

	// set config
	function sf_get_config($args) {
		
		// mobile: force disable preview pane.
		if ($this->mobile && $args["name"] == "preview_pane") {
			$args['result'] = false;
			return $args;
		}

		// force compatibility saying we are using larry.
		if ($args["name"] != "skin" || !in_array($args["result"], $this->problematic_skins)) {
			return $args;
		}
		
		$trace  = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);

		if (!empty($trace[3]["file"]) && in_array(basename(dirname($trace[3]["file"])), $this->problematic_plugins)) {
			$args["result"] = "larry";
		}

		return $args;
	}

	/* start */
	public function sf_start() {
		$rcmail = rcmail::get_instance();


		$this->sf_set_skin();
	}

	// modify html
	public function sf_render_page($html) {
		if ($this->phone) {
			$class = "sf-device-mobile sf-device-phone";
		} else if ($this->tablet) {
			$class = "sf-device-mobile sf-device-tablet";
		} else {
			$class = "sf-device-desktop";
		}

		$html["content"] = str_replace("</body>", "<script>jQuery('body').addClass('" . $class . "');</script></body>", $html['content']);

		return $html;
	}

	// set the skin
	function sf_set_skin() {
		$rcmail = rcmail::get_instance();
		$prefs = $rcmail->user->get_prefs();

		$this->skin = isset($prefs['skin']) ? $prefs['skin'] : $rcmail->config->get('skin');
		$this->skin || $this->skin = "larry";

		$this->phone_skin = isset($prefs['phone_skin']) ? $prefs['phone_skin'] : $rcmail->config->get('phone_skin');
		$this->tablet_skin = isset($prefs['tablet_skin']) ? $prefs['tablet_skin'] : $rcmail->config->get('tablet_skin');
		$this->desktop_skin = isset($prefs['desktop_skin']) ? $prefs['desktop_skin'] : $rcmail->config->get('desktop_skin');

		$this->phone_skin || $this->phone_skin = $this->skin;
		$this->tablet_skin || $this->tablet_skin = $this->skin;
		$this->desktop_skin || $this->desktop_skin = $this->skin;

		if ($this->phone) {
			$this->skin = $this->phone_skin;
		} else if ($this->tablet) {
			$this->skin = $this->tablet_skin;
		} else {
			$this->skin = $this->desktop_skin;
		}

		if (method_exists($GLOBALS['OUTPUT'], "set_skin")) {
			$GLOBALS['OUTPUT']->set_skin($this->skin);
		}
	}

	/* Detect device */
	private function set_device() {
		require_once("Mobile_Detect.php");
		$detect = new Mobile_Detect();

		$this->mobile = $detect->isMobile();
		$tablet = $detect->isTablet();

		if (isset($_GET['phone'])) {
			$this->phone = (bool)$_GET['phone'];
		} else {
			$this->phone = $this->mobile && !$tablet;
		}

		if (isset($_GET['tablet'])) {
			$this->tablet = (bool)$_GET['tablet'];
		} else {
			$this->tablet = $tablet;
		}

		$this->desktop = !$this->mobile;

		if ($this->phone) {
			$this->device = "phone";
		} else if ($this->tablet) {
			$this->device = "tablet";
		} else {
			$this->device = "desktop";
		}
	}

	// display preferences
	private function skin_item($type, $skin, $skin_name, $thumbnail, $author, $license, $selected) {
		return
			html::div(array('class'=>"skinselection" . ($selected ? " selected" : "")),
				html::a(array('href'=>'javascript:void(0)', 'onclick'=>"device_skins.dialog('$type', '$skin', this)"),
					html::span(
						'skin_item',
						"<input type='hidden' value='$skin' />".
						html::img(array('src'=>$thumbnail, 'class'=>'skinthumbnail', 'alt'=>$skin, 'width'=>64, 'height'=>64))
					) .
					html::span(
						'skin_item',
						"<input type='hidden' value='$skin' />".
						html::span('skinname', Q($skin_name)
						) .
						html::br() .
						html::span('skinauthor', $author ? 'by ' . $author : '') .
						html::br() .
						html::span('skinlicense', $license ? rcube_label('license').':&nbsp;' . $license : ''))
				));
	}

	// hide the old skin select display
	function sf_preferences_list($args) {

		if ($args['section'] != 'general' || !isset($args['blocks']['skin'])) {
			return $args;
		}

		// check if skin is define to not overwrite it.
		global $RCMAIL;
		$no_override = array_flip($RCMAIL->config->get('dont_override', array()));
		if (isset($no_override['skin'])) {
			return $args;
		}

		$config = $RCMAIL->config->all();
		$skins = rcmail_get_skins();

		if (count($skins) <= 1) {
			return $args;
		}

		sort($skins);

		$this->add_texts('localization');

		// remove old 'select skin' block
		unset($args['blocks']['skin']);

		// create skin selection hidden blocks that will be shown in dialogs, if mobile, create the selects
		// since we don't use dialogs in mobile

		if ($this->desktop) {
			$desktop_list = "";
			$tablet_list = "";
			$phone_list = "";
		} else {
			$desktop_select = new html_select(array("name"=>"_skin", "id"=>"rcmfd_skin"));
			$tablet_select = new html_select(array("name"=>"_tablet_skin", "id"=>"rcmfd_tablet_skin"));
			$phone_select = new html_select(array("name"=>"_phone_skin", "id"=>"rcmfd_phone_skin"));
		}

		foreach ($skins as $skin) {

			$thumbnail = "./skins/$skin/thumbnail.png";
			if (!is_file($thumbnail)) {
				$thumbnail = './program/resources/blank.gif';
			}

			$skin_name = ucfirst($skin);
			$author = "";
			$license = "";
			$meta = @json_decode(@file_get_contents("./skins/$skin/meta.json"), true);

			if (is_array($meta) && $meta['name']) {
				$skin_name = $meta['name'];
				$author  = $meta['author'];
				$license = $meta['license'];
			}

			if ($this->desktop) {

				$selected = $skin == $this->desktop_skin;
				$item = $this->skin_item("desktop", $skin, $skin_name, $thumbnail, $author, $license, $selected);
				$desktop_list .= $item;

				if ($selected) {
					$desktop_select = $item;
				}

				$selected = $skin == $this->tablet_skin;
				$item = $this->skin_item("tablet", $skin, $skin_name, $thumbnail, $author, $license, $selected);
				$tablet_list .= $item;

				if ($selected) {
					$tablet_select = $item;
				}

				$selected = $skin == $this->phone_skin;
				$item = $this->skin_item("phone", $skin, $skin_name, $thumbnail, $author, $license, $selected);
				$phone_list .= $item;

				if ($selected) {
					$phone_select = $item;
				}
			} else {
				$desktop_select->add($skin_name, $skin);
				$tablet_select->add($skin_name, $skin);
				$phone_select->add($skin_name, $skin);
			}
		}

		if ($this->desktop) {

			if (!$desktop_select) {
				$desktop_select = "<a href='javascript:void(0)' onclick='sf_device_skins.dialog(\"desktop\", \"\", this)'>" .
					Q($this->gettext("select")) . "</a>";
			}

			if (!$tablet_select) {
				$tablet_select = "<a href='javascript:void(0)' onclick='sf_device_skins.dialog(\"tablet\", \"\", this)'>" .
					Q($this->gettext("select")) . "</a>";
			}

			if (!$phone_select) {
				$phone_select = "<a href='javascript:void(0)' onclick='sf_device_skins.dialog(\"phone\", \"\", this)'>" .
					Q($this->gettext("select")) . "</a>";
			}

			$desktop_select = "<div class='skin-select' id='desktop-skin-select'>$desktop_select</div>".
				"<div class='skin-list' id='desktop-skin-list' title='" . Q($this->gettext("select_desktop_skin")) . "'>".
				$desktop_list.
				"</div>";

			$tablet_select = "<div class='skin-select' id='tablet-skin-select'>$tablet_select</div>".
				"<div class='skin-list' id='tablet-skin-list' title='" . Q($this->gettext("select_tablet_skin")) . "'>".
				$tablet_list.
				"</div>";

			$phone_select = "<div class='skin-select' id='phone-skin-select'>$phone_select</div>".
				"<div class='skin-list' id='phone-skin-list' title='" . Q($this->gettext("select_phone_skin")) . "'>".
				$phone_list.
				"</div>".

				"<div id='skinPost'>".
				"<input id='desktop-skin-post' type='hidden' name='_skin' value='{$this->desktop_skin}' />".
				"<input id='tablet-skin-post' type='hidden' name='_tablet_skin' value='{$this->tablet_skin}' />".
				"<input id='phone-skin-post' type='hidden' name='_phone_skin' value='{$this->phone_skin}' />".
				"</div>";
		} else {
			$desktop_select = $desktop_select->show($this->desktop_skin);
			$tablet_select = $tablet_select->show($this->tablet_skin);
			$phone_select = $phone_select->show($this->phone_skin);
		}

		if ($this->desktop) {
			$args['blocks']['skin']['name'] = $this->gettext("legend");
		} else {
			$args['blocks']['skin']['name'] = Q(rcube_label('skin'));
		}

		$args['blocks']['skin']['options']['desktop_skin'] = array('title'=>$this->gettext("desktop"), 'content'=>$desktop_select);

		$args['blocks']['skin']['options']['tablet_skin'] = array('title'=>$this->gettext("tablet"), 'content'=>$tablet_select);

		$args['blocks']['skin']['options']['phone_skin'] = array('title'=>$this->gettext("phone"), 'content'=>$phone_select);

		return $args;
	}

	// save preferences.
	function sf_preferences_save($args) {
		if ($args['section'] != 'general') {
			return $args;
		}

		$args['prefs']['desktop_skin'] = get_input_value('_skin', RCUBE_INPUT_POST);
		$args['prefs']['tablet_skin'] = get_input_value('_tablet_skin', RCUBE_INPUT_POST);
		$args['prefs']['phone_skin'] = get_input_value('_phone_skin', RCUBE_INPUT_POST);

		if ($this->phone) {
			$args['prefs']['skin'] = $args['prefs']['phone_skin'];
		} else if ($this->tablet) {
			$args['prefs']['skin'] = $args['prefs']['tablet_skin'];
		} else {
			$args['prefs']['skin'] = $args['prefs']['desktop_skin'];
		}

		return $args;
	}
}

